Discord bot that relays chat messages between a minecraft server and a discord server

# Requirements
For Relaying messages to minecraft server a server management script is used.
The script can be downloaded 
[here](https://github.com/Edenhofer/minecraft-server/).

# Usage
## Config File
To use this a config.json file must be created. In the config.json there needs to be 4 elements. They are channelID, which is the channelID in which the bot will relay the minecraft messages to, token is the token for the discord bot, logFile is the the path to the latest.log log file of the minecraft server and tellrawCommand is the shell command used to relay the messages to discord. An example of a config.json file is found below.
``` json
{
    "clientId": "123456",
    "token": "69adjc420",
    "logFile": "/path/to/server/logs/latest.log",
    "tellrawCommand": "papermc command /tellraw @a "
    "activity": "Do you know what is under there?"
}
```

## Deploying Commands
The bot uses slash commands to register channels. Before the bot is run remember to run the deploy-command.js file. Once successful it will say that the commands are succefully registered. The deploy-command.js file can be run by doing: 

``` sh
node deploy-commands.js
```

## Registering Channels
In order for the bot to work, channels must be registered to the bot. To do that, the `/add_channel` command on discord can be used. This will add the channel the command used it in to a channels.json file. Users can also specify which channel to add by passing in a channel like `/add_channel #general`. The bot will even work on multiple channels and can even work relay messages between discord servers. Do note that channels not in the server can't added.

## Removing Channels
To remove a channel from the registered channel list, the `/remove_channel` command can be used. By default it will remove the channel where the command is called if no channels is passed in. However, specific channels can be removed by passing in as an option `/remove_channel #general`. Do note that channels not in the same server can't be removed. To get all the channels in the server registered, the `/get_channels` command can be used. This will send a message with all the channels currently registered by the bot.

## Installing Requirements
There is only 1 main requirements needed for this script to work, it is `discord.js`. To install it do: 

``` sh
npm install discord.js
```

or if you are currently in the directory of the git repository

``` sh
npm install
```

## Running Bot
To start the discord bot simply run.

``` sh
node index.js
```

or if you are currently in the directory of the git repository

``` sh
npm run test
```

# TODO
* [x] Continue working after server restarts
* [ ] Starts and stops with the server
* [x] Add support for announcments (chat messages that starts with [_player_]) other than the server
* [x] Add interaction to setup which channel to send to
