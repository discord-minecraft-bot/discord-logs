const { SlashCommandBuilder } = require('@discordjs/builders');
const fs = require('node:fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('add-channel')
    .setDescription('Adds channel to relay message to')
    .addChannelOption(option =>
      option.setName('id')
        .setDescription('Channel ID to add, defaults to current channel if id not specified')
        .setRequired(false)),
  async execute(interaction) {
    const channels = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
    const channelIds = channels.channelIds;
    const guildIds = channels.guildIds;
    // Getting the guild and channel ID to add
    let option = interaction.options.get('id');
    let channel = "";
    let guild = "";
    if (!option) {
      channel = interaction.channelId;
      guild = interaction.guildId;
    }
    else {
      channel = option.channel.id;
      guild = option.channel.guild.id;
    }

    // Checking if it is already on the list
    for (var id of channelIds) {
      if (channel == id) {
        await interaction.reply('Channel already in list. Skipping...');
        return;
      }
    }

    // Adding ID to the list
    channelIds.push(channel);
    guildIds.push(guild);
    let newChannels = { guildIds, channelIds };
    fs.writeFileSync('./channels.json', JSON.stringify(newChannels, null, 2));

    // Confirmation reply
    await interaction.reply(`Added <#${channel}> to channel list`);
  },
};
