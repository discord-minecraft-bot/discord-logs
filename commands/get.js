const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js')
const fs = require('node:fs');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('get-channels')
    .setDescription('Gets all channels the bot is curretly relaying message to'),
  async execute(interaction) {
    const channels = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
    const channelIds = channels.channelIds;
    const guildIds = channels.guildIds;

    // Getting all the channel in the guild
    let guildId = interaction.guildId;
    let channelList = new Array();
    for (var i in channelIds) {
      if (guildIds[i] == guildId) {
        channelList.push("<#" + channelIds[i] + ">");
      }
    }
    let channelValue = channelList.join("\n");
    if (!channelValue) {
      channelValue = "No channels found!";
    }

    // Setting up embed
    const channelEmbed = new MessageEmbed()
      .setTitle("Registered channels in the server")
      .addField("Channels:", channelValue);
    // Reply with channels
    await interaction.reply({ embeds: [channelEmbed] });
  },
};
