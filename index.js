#!/usr/bin/env node

const { Client, Collection, Intents } = require('discord.js');
const { token } = require("./config.json");
const { logFile, tellrawCommand, activity } = require('./config.json');
const { spawn } = require('node:child_process');
const fs = require('fs');
const path = require('node:path');

// Creating channels.json
try {
  fs.readFileSync('channels.json');
} catch (err) {
  // Do something
  let emptyChannels = {
    "guildIds": [],
    "channelIds": []
  }
  fs.writeFileSync('channels.json', JSON.stringify(emptyChannels, null, 2));
}
// Function to delete deleted channels
function delete_channel(channel) {
  const channels = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
  const channelIds = channels.channelIds;
  const guildIds = channels.guildIds;

  const index = channelIds.indexOf(channel);
  if (index > -1) {
    channelIds.splice(index, 1); // 2nd parameter means remove one item only
    guildIds.splice(index, 1); // 2nd parameter means remove one item only
  }
  // channelIds.filter((value, i) => i != index)
  // guildIds.filter((value, i) => i != index)
  let newChannels = { guildIds, channelIds };
  fs.writeFileSync('./channels.json', JSON.stringify(newChannels, null, 2));
  console.log(`Deleting ${channel} as it doesn't exist.`)
}

// Create a new client instance
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
  ]
});

// When the client is ready, run this code (only once)
client.once('ready', () => {
  console.info('Discord Ready!');
  // Setting Status
  client.user.setActivity(activity);
  watchLog(logFile);
});

// When a message is ent
client.on('messageCreate', async (message) => {
  const channels = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
  const channelIds = channels.channelIds;
  // Listen for chat messages from discord and echo them to minecraft
  // Do nothing if message coming from it self
  if (message.author.id == client.user.id) return;
  if (!channelIds.includes(message.channelId)) return;

  // Formatting and printing message
  let sentMessage = `[${message.guild.name}]` +
    "[" + message.author.username + "@" + message.channel.name + "] "
    + message.content;
  console.log(sentMessage);

  // Sending it to minecraft
  let tellrawList = tellrawCommand.split(' ');
  let shellCommand = tellrawList[0];
  let args = tellrawList.slice(1);
  args.push(`\"${sentMessage}\"`);
  spawn(shellCommand, args);

  channelIds.forEach(channelID => {
    const channel = client.channels.cache.get(channelID);
    if (channelID != message.channelId && channel)
      channel.send(sentMessage);
    else if (!channel)
      delete_channel(channelID);
  })
});

// Bot Commands
// Registering Commands
client.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  const command = require(filePath);
  // Set a new item in the Collection
  // With the key as the command name and the value as the exported module
  client.commands.set(command.data.name, command);
}

client.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return;

  const command = client.commands.get(interaction.commandName);

  if (!command) return;

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
  }
});

// Disconnection
client.once('shardDisconnect', (event, id) => {
  console.error("Discord Disconnected");
  console.error("Event:", event, "ID:", id);
  process.exit(1);
});

// Login to Discord with your client's token
client.login(token);

// https://stackoverflow.com/questions/11225001/reading-a-file-in-real-time-using-node-js
// https://stackoverflow.com/questions/50716044/nodejs-get-last-line-of-file-everytime-file-is-changed
// Reading log file
var readbytes = 0,
  file;
function watchLog(logFile) {
  var stats = fs.statSync(logFile); // yes sometimes async does not make sense!
  readbytes = stats.size;

  let watcher = fs.watch(logFile, (eventType, filename) => {
    if (eventType == "rename") {
      watcher.close();
    } else if (eventType == "change") {
      fs.open(logFile, 'r', function(err, fd) {
        if (err) {
          watcher.close();
          return;
        }
        readsome(fd);
      });
    }
  });

  function testlogFile(logFile) {
    setTimeout(() => {
      try {
        watchLog(logFile);
      } catch (err) {
        testlogFile(logFile);
      }
    }, 100)
  }

  watcher.once('close', () => {
    testlogFile(logFile);
  });
}

function readsome(file) {
  var stats = fs.fstatSync(file); // yes sometimes async does not make sense!

  if (stats.size < readbytes) {
    console.log(logFile, "changed. Reseting readbytes.");
    readbytes = stats.size;
    let bite_size = stats.size, bite_size_array = new Array(bite_size);
    fs.read(file, Buffer.from(bite_size_array), 0, bite_size, readbytes, processsome);
  } else {
    let bite_size = stats.size - readbytes, bite_size_array = new Array(bite_size);
    fs.read(file, Buffer.from(bite_size_array), 0, bite_size, readbytes, processsome);
  }
}

function processsome(err, bytecount, buff) {
  if (err) console.error("Error reading bytes");
  console.log('Read', bytecount, 'and will process it now.');

  // Here we will process our incoming data:
  // Do whatever you need. Just be careful about not using beyond the bytecount in buff.
  let newData = buff.toString('utf-8', 0, bytecount);
  let newLines = newData.split('\n');
  let loggedContents = new Array();
  newLines.forEach((element) => {
    loggedContents.push(element)
  });

  // Checking if it is something to send in chat
  for (var loggedContent of loggedContents) {
    loggedContent = loggedContent.split(':').slice(3).join(":").trim()
    if (/^<.*>/.test(loggedContent) || /^\[.*\]/.test(loggedContent) ||
      /has completed the challenge \[.*\]$/.test(loggedContent) || /has made the advancement \[.*\]$/.test(loggedContent) ||
      /joined the game$/.test(loggedContent) || /left the game$/.test(loggedContent)) {
      // Logging and sending it to discord
      console.log(loggedContent);

      const channels = JSON.parse(fs.readFileSync('./channels.json', 'utf-8'));
      const channelIds = channels.channelIds;
      channelIds.forEach(channelID => {
        const channel = client.channels.cache.get(channelID);
        if (channel)
          client.channels.cache.get(channelID).send(loggedContent);
        else
          delete_channel(channelID);
      })
    }
  }

  // So we continue reading from where we left:
  readbytes += bytecount;
}
